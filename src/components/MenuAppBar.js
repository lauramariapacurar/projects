import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';


const styles = {
    root: {
        flexGrow: 1,
    },
};

function MenuAppBar(props) {
    const { classes } = props;

    return (
        <div className={classes.root}>
            <AppBar position="fixed" color="default">
                <Toolbar >

                    <Typography variant="body1" style={{ margin:'1%',marginRight:'13%'}}>
                        LOGO
                    </Typography>
                    <Typography variant="body1" style={{color:'blue', margin:'1%'}}>
                        Home
                    </Typography>
                    <Typography variant="body1" style={{ margin:'1%'}}>
                        Documents
                    </Typography>
                    <Typography variant="body1" style={{ margin:'1%'}}>
                        Learning Center
                    </Typography>
                    <Typography variant="body1" style={{ margin:'1%'}}>
                        Videos
                    </Typography>
                    <Typography variant="body1" style={{ margin:'1%'}}>
                        Safe Connect
                    </Typography>
                    <Typography variant="body1" style={{ margin:'1%',marginLeft:'33%'}}>
                        AVATAR
                    </Typography>
                </Toolbar>

            </AppBar>
        </div>
    );
}

MenuAppBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuAppBar);