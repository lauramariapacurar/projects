import React, {Component} from 'react';
import AlertItem from "components/AlertItem";


class AlertList extends Component{

    constructor()
    {
        super();
        this.state = {
            'items': []
        };

    }
    componentDidMount() {
    this.getItems();
     }

     getItems(){
         fetch('https://jsonplaceholder.typicode.com/todos')
             .then(response => response.json())
             .then(json => this.setState({'items': json}));

}

 render(){


     return (
         <div style={{backgroundColor:'white', padding:'10px'}}>
             <h3 style={{fontFamily: 'Helvetica', color:'#222222', fontWeight:'300'}}>Alerts</h3>

             {this.state.items.map(function (item,index) {
                 return <AlertItem item={item} key={index}/>

             })}
         </div>
     );
 }



}

export default AlertList;