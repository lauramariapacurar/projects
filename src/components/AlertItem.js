import React ,{ Component } from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import Image from 'material-ui-image';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';


class AlertItem extends Component{
    render(){
        return (
            <div style={{backgroundColor:'white', padding: "5px", margin: "1px", borderTop:'1' +
                    'px solid #222222'}} >
            <Grid container spacing={24} style={{fontFamily: 'Helvetica', color:'#071a1c', fontWeight: '300',lineHeight: '22px'}}>
                <Grid item xs={12} sm={2} md={2}>
                    {/*<Image*/}
                        {/*src={require('../assets/Poza2.jpeg')}*/}
                    {/*/>*/}
                    <Image src='https://fakeimg.pl/100x100/' imageStyle={{width:'48px', height:'auto'}} />

                </Grid>
                <Grid item xs={12} sm={10} md={8}>
                    <div style={{fontSize:'14px'}}>
                        <h4 style={{fontSize: '22px', lineHeight:'26px', fontWeight: '300', color:'#c3c3c3',}}>{this.props.item.title}</h4>
                        Descriere test test <br/>
                         10/11/2018 to 10/12/2018- <span style={{color:'#67d360'}}> <i>Approved</i> </span>

                    </div>
                </Grid>
                <Grid item xs={12} sm={12} md={2} style={{marginTop:'15%'}} >
                    <StyledButton1 > Remove</StyledButton1> <br/>
                    <StyledButton2 > Override</StyledButton2>
                </Grid>

            </Grid>
            </div>
        );
    }
}

const StyledButton1 = withStyles({
    root: {
        background: '#ca5c5c',
        minHeight:'20px',
        border: 0,
        color: 'white',
        padding: '0 6px',
        margin:'2%',
    },
    label: {
        textTransform: 'capitalize',
    },
})(Button);

const StyledButton2 = withStyles({
    root: {
        background: '#2f2f2f',
        minHeight:'20px',
        border: 0,
        color: '#4fc6d3',
        padding: '0 6px',
        fontSize:'12px',
        margin:'2%',

    },
    label: {
        textTransform: 'capitalize',
        fontSize:'12px',
    },
})(Button);

export default AlertItem;