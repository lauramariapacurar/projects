import React from 'react';
import AlertList from '../../components/AlertList';
import Grid from "@material-ui/core/Grid/Grid";
import MenuAppBar from "components/MenuAppBar";



class ComAlerts extends React.Component {

    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div style={{backgroundColor:"#e4e4ce"}}>
               <MenuAppBar />


                <Grid container spacing={24} style={{marginTop:'5%'}}>
                    <Grid item xs={6}>
                        <div> CE FACE BWIN </div>
                    </Grid>
                    <Grid item xs={6}>
                        <AlertList />
                    </Grid>

                </Grid>


            </div>

        );
    }
}

export default ComAlerts;