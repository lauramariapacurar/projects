import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './Routes';
import 'theme/scss/app.scss';

const render = Component => {
    ReactDOM.render(<Component/>, document.getElementById('app'));
};

render(Routes);

if (module.hot) {
    module.hot.accept(Routes, () => {
        const NextApp = require('./pages/Routes');
        render(NextApp);
    });
}


