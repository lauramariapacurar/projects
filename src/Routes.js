import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

import {ThemeProvider} from 'styled-components';
import theme from 'theme';
import GeneralLayout from "layouts/GeneralLayout";

import ComAlerts from "pages/ComAlerts";

const Routes = () => {
    return (
        <ThemeProvider theme={theme}>
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={GeneralLayout(ComAlerts)}/>
                </Switch>
            </BrowserRouter>
        </ThemeProvider>
    )
};

export default Routes;