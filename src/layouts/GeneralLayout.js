import React from 'react';

const GeneralLayout = Page => {
    return props => <Page {...props}/>
};

export default GeneralLayout;