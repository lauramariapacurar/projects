import variables from "theme/scss/variables.scss";

const theme = {
    color: {
        red: variables.red,
        primary: variables.primary,
        secondary: variables.secondary,
        cream: variables.cream,
        brown: variables.brown
    }
};
export default theme;